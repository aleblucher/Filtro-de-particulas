#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from pf import Particle, create_particles, draw_random_sample
import numpy as np
import inspercles # necessário para o a função nb_lidar que simula o laser
import math
from scipy.stats import norm
import random

largura = 775 # largura do mapa
altura = 748  # altura do mapa

# Robo
robot = Particle(largura/2, altura/2, math.pi/4, 1.0)

# Nuvem de particulas
particulas = []

num_particulas = 500


# Os angulos em que o robo simulado vai ter sensores
angles = np.linspace(0.0, 2*math.pi, num=8, endpoint=False)

# Lista mais longa
movimentos_longos = [[-10, -10, 0], [-10, 10, 0], [-10,0,0], [-10, 0, 0],
              [0,0,math.pi/12.0], [0, 0, math.pi/12.0], [0, 0, math.pi/12],[0,0,-math.pi/4],
              [-5, 0, 0],[-5,0,0], [-5,0,0], [-10,0,0],[-10,0,0], [-10,0,0],[-10,0,0],[-10,0,0],[-15,0,0],
              [0,0,-math.pi/4],[0, 10, 0], [0,10,0], [0, 10, 0], [0,10,0], [0,0,math.pi/8], [0,10,0], [0,10,0],
              [0,10,0], [0,10,0], [0,10,0],[0,10,0],
              [0,0,-math.radians(90)],
              [math.cos(math.pi/3)*10, math.sin(math.pi/3),0],[math.cos(math.pi/3)*10, math.sin(math.pi/3),0],[math.cos(math.pi/3)*10, math.sin(math.pi/3),0],
              [math.cos(math.pi/3)*10, math.sin(math.pi/3),0]]

# Lista curta
movimentos_curtos = [[-10, -10, 0], [-10, 10, 0], [-10,0,0], [-10, 0, 0]]

movimentos_relativos = [[0, -math.pi/3],[10, 0],[10, 0], [10, 0], [10, 0],[15, 0],[15, 0],[15, 0],[0, -math.pi/2],[10, 0],
                       [10,0], [10, 0], [10, 0], [10, 0], [10, 0], [10, 0],
                       [10,0], [10, 0], [10, 0], [10, 0], [10, 0], [10, 0],
                       [10,0], [10, 0], [10, 0], [10, 0], [10, 0], [10, 0],
                       [0, -math.pi/2],
                       [10,0], [10, 0], [10, 0], [10, 0], [10, 0], [10, 0],
                       [10,0], [10, 0], [10, 0], [10, 0], [10, 0], [10, 0],
                       [10,0], [10, 0], [10, 0], [10, 0], [10, 0], [10, 0],
                       [10,0], [10, 0], [10, 0], [10, 0], [10, 0], [10, 0],
                       [0, -math.pi/2],
                       [10,0], [0, -math.pi/4], [10,0], [10,0], [10,0],
                       [10,0], [10, 0], [10, 0], [10, 0], [10, 0], [10, 0],
                       [10,0], [10, 0], [10, 0], [10, 0], [10, 0], [10, 0]]



movimentos = movimentos_relativos



def cria_particulas(minx=0, miny=0, maxx=largura, maxy=altura, n_particulas=num_particulas):
    variacao_x = (maxx-minx)/2
    variacao_y = (maxy-miny)/2

    return create_particles(robot.pose(),var_x = variacao_x,var_y = variacao_y, num = num_particulas)


def move_particulas(particulas, movimento):
    for i in particulas:
        i.move_relative(movimento)

    return particulas

def leituras_laser_evidencias(robot, particulas):
    leitura_particulas = []
    sigma = 9.7
    for i in particulas:
        leitura_particulas.append(inspercles.nb_lidar(i,angles))
    leitura_robo = inspercles.nb_lidar(robot, angles)
    soma_w = 0
    for i in range(len(particulas)):
        w_after = 0
        for angulo in angles:
            w_after += norm.pdf(leitura_particulas[i][angulo], loc = leitura_robo[angulo], scale = sigma)
        particulas[i].w = w_after
        soma_w += w_after

    alpha = 1 / soma_w

    for i in particulas:
        i.w *= alpha

def reamostrar(particulas, n_particulas = num_particulas):
    probabilities = []
    for i in particulas:
        probabilities.append(i.w)

    particulas = draw_random_sample(particulas,probabilities,n_particulas)

    for i in particulas:
        i.w = 1 / n_particulas
        i.x += random.randrange(-10,10,1)/10
        i.y += random.randrange(-10,10,1)/10
        i.theta += random.randrange(-10,10,1)/10


    return particulas
